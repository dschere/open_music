# open-music
A collection of public domain music composions using the tuxguitar (http://tuxguitar.com.ar/) music composition program on Linux.

You are free to use and/or contribute to these. 

# optional steps
I recommend using a dfferent sound font from the one that normally comes with linux
since the midi voice for the guitar is horrible for the default sound font. 
 
If you want to use the soundfont that I used do the following in tuxguitar 
    Tools->Plugins->Java Sound API Plugin
       configure
          select checkbox -> custom soundbank
             /path/to/this/git/sound-fonts/27mg_Symphony_Hall_Bank.SF2

For generating the mp3 files I used the Linux Audio recorder which captures what is 
normally sent to speakers to disk. I then just played the compositions and had this
tool record mp3 files.

# songs
The songs directory contains compositions.
  
   
Enjoy.
   


